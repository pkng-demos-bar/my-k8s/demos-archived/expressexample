var express = require('express');
var router = express.Router();

/*
const { Sequelize } = require('sequelize');
const sequelize = new Sequelize('sqlite::memory:')
const util = require('util')

router.get('/', (req, res) => {
  // ruleid: tainted-sql-string
  const query = "SELECT * FROM `users`" + " WHERE id = '" + req.query.message + "'"
  const [results, metadata] = await sequelize.query(query);
  res.send(results)
})

*/


// File path traversal 
// https://gitlab.com/gitlab-org/secure/gsoc-sast-vulnerability-rules/playground/sast-rules/-/blob/main/javascript/pathtraversal/rule-non_literal_fs_filename.yml
const fs = require('fs')
const path = require('path');
const rootdir = '/var/www/';

router.get('/directory', (req,res) => {

  //user input file name contains the filename which will be given by the user
  let filename = path.join(rootdir, req.query.userinputfilename); 

  try {
    if (fs.existsSync(path)) {
    //file exists
    }
  } catch(err) {
    console.error(err)
  }
 
  res.send('test');

})


// Weak random
//https://gitlab.com/gitlab-org/secure/gsoc-sast-vulnerability-rules/playground/sast-rules/-/blob/main/javascript/random/test-pseudo_random_bytes.js
const crypto = require('crypto');
router.get('/random', (req,res) => {
  res.send(crypto.pseudoRandomBytes(4).toString('hex'));
})


module.exports = router;
